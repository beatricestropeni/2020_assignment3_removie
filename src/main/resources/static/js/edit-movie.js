function viewRemake() {
	var checkBox = document.getElementById("checkboxRemake");
	var remakeSelect = document.getElementById("remake");
	var yesNoLabel = document.getElementById("yesno");
	
	if(checkBox.checked == true) {
		remakeSelect.disabled = false;
		yesNoLabel.innerHTML="Si";
	}
	else {
		remakeSelect.disabled = true;
		yesNoLabel.innerHTML="No";
	}
}

$(document).ready(function() {	
	
	$("#form").on("submit", function() {
		var remakeId = null;
		var actorCheckbox = document.getElementsByName('actorCheckbox');
		
		var actorIdChecked = [];
		
		//inserisce nell'array actorIdChecked l'id degli attori che partecipano al film
		for (var i = 0; i < actorCheckbox.length; i++) {
		    if (actorCheckbox[i].type == 'checkbox' && actorCheckbox[i].checked == true) {
		    	actorIdChecked.push(actorCheckbox[i].value)
		    }
		}
		
		
		if(actorIdChecked.length != 0) {
			
			//estrae l'id del remake, se è stato inserito
			if ($('#checkboxRemake').is(':checked')) {
				remakeId = $("#remake option:selected").val()
				console.log(remakeId);
				if(remakeId == $("#id").val()) {
					$('#modalRemakeError').modal(); 
					return false;
				}
			}
			
			//creo il json
			var movie = {
					title : $("#title").val(),
					runningTime : $("#runningTime").val(),
					productionCompany : $("#productionCompany").val(),
					releaseDate : $("#releaseDate").val(),
					plot : $("#plot").val(),
					genre: $("#genre option:selected").text(),
					remakeId: remakeId,
					directorId: $("#director option:selected").val(),
					actors: actorIdChecked
			}
			
			// DO POST
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "edit",
				data : JSON.stringify(movie),
				dataType : 'json',
				success: function (data) {
	                var obj = data;
	                console.log("SUCCESS: ", obj);
	                window.location = "/movies";
	                return false;
	            },
				error : function(e) {
					console.log("ERROR: ", e);
					window.location = "/movies";
					return false;
				}
			});
			
			return false;
			
		}
		else {
			//errore: bisogna inserire almeno un attore
			$('#myModal').modal(); 
			return false;
		}
	});
});