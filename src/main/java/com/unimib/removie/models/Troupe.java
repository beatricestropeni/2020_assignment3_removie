package com.unimib.removie.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Troupe {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	private String name;
	private String surname;
	@Column(name = "birth_date")
	private LocalDate birthDate = LocalDate.now();
	private String nationality;
	
	protected Troupe() {
	}

	protected Troupe(String name, String surname, LocalDate birthDate, String nationality) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.nationality = nationality;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getBirthDate() {
		return birthDate.toString();
	}
	
	public void setBirthDate(String date){
		DateTimeFormatter f = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		this.birthDate = LocalDate.parse( date , f ) ;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public int getId() {
		return id;
	}
	
}
