package com.unimib.removie.models;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Movies")
public class Movie {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	private String title;
	@Column(name = "running_time")
	private int runningTime;
	private Genre genre;
	@Column(name = "production_company")
	private String productionCompany;
	@Column(name = "release_date")
	private int releaseDate;
	private String plot;
	@ManyToOne(/*optional = true, fetch = FetchType.EAGER*/)
	private Movie remake;
	@ManyToOne()
	@JoinColumn(name="director_id", nullable=false)
	private Director director;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Movies_Actors")
	private List<Actor> actors;
	
	public Movie() {
	}

	public Movie(String title, int runningTime, Genre genre, String productionCompany, 
			int releaseDate,
			String plot, Movie remake, Director director, List<Actor> actors) {
		super();
		this.title = title;
		this.runningTime = runningTime;
		this.genre = genre;
		this.productionCompany = productionCompany;
		this.releaseDate = releaseDate;
		this.plot = plot;
		this.remake = remake;
		this.director = director;
		this.actors = actors;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getRunningTime() {
		return runningTime;
	}

	public void setRunningTime(int runningTime) {
		this.runningTime = runningTime;
	}

	public String getProductionCompany() {
		return productionCompany;
	}

	public void setProductionCompany(String productionCompany) {
		this.productionCompany = productionCompany;
	}

	public int getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(int releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Genre getGenre() {
		return genre;
	}
	
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	public List<Actor> getActors() {
		return actors;
	}

	public void addActor(Actor actor) {
		this.actors.add(actor);
	}
	
	public void deleteAttore(Actor actor) {
		this.actors.remove(actor);
	}

	public Movie getRemake() {
		return remake;
	}

	public void setRemake(Movie remake) {
		this.remake = remake;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", running_time=" + runningTime + ", genre=" + genre
				+ ", production_company=" + productionCompany + ", release_date=" + releaseDate + ", plot=" + plot
				+ ", remake=" + remake + ", director=" + director + ", actors=" + actors + "]";
	}	
	
}
