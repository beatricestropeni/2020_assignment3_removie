package com.unimib.removie.models;

import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name="Reviews")
public class Review {
	@Id
	@GeneratedValue
	private int id;
	@ManyToOne
	@JoinColumn(name="member_id")
	private Troupe member;
	@ManyToOne
	@JoinColumn(name="movie_id")
	private Movie movie;
	private String description;
	private int rating;
	@ManyToOne
	@JoinColumn(name="user_username")
	private User user;
	
	public Review() {
	
	}
	
	public Review(Troupe member, Movie movie, String description, int rating, User user) {
		super();
		this.member = member;
		this.movie = movie;
		this.description = description;
		this.rating = rating;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Troupe getMember() {
		return member;
	}

	public void setMember(Troupe member) {
		this.member = member;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
