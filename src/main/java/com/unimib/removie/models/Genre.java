package com.unimib.removie.models;

public enum Genre {
		DRAMMATICO("DRAMMATICO"),
		COMMEDIA("COMMEDIA"),
		ANIMAZIONE("ANIMAZIONE"),
		AVVENTURA("AVVENTURA"),
		BIOGRAFICO("BIOGRAFICO"),
		DOCUMENTARIO("DOCUMENTARIO"),
		FANTASY("FANTASY"),
		GUERRA("GUERRA"),
		HORROR("HORROR"),
		MUSICAL("MUSICAL"),
		STORICO("STORICO"),
		THRILLER("THRILLER"),
		WESTERN("WESTERN");
	
	private final String name;
	
	protected static final Genre[] ALL= {DRAMMATICO,COMMEDIA,ANIMAZIONE,AVVENTURA,BIOGRAFICO,DOCUMENTARIO,FANTASY,GUERRA,HORROR,MUSICAL,STORICO,THRILLER,WESTERN};
	
	private Genre(final String name) {
        this.name = name;
    }
	
	public static Genre[] getAllGenre() {
		return Genre.ALL;
	}
	
	 public static Genre getGenreByName(String name) {
		 for(int i = 0; i < Genre.ALL.length; i++) {
			 if(Genre.ALL[i].name.equals(name))
				 return Genre.ALL[i];
		 }
		 return null;
    }
}	