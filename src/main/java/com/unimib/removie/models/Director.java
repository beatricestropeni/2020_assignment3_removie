package com.unimib.removie.models;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name="Directors")
public class Director extends Troupe {
	@Column(name = "is_producer")
	private boolean isProducer;
	@Column(name = "favourite_genre")
	private Genre favouriteGenre;
	
	public Director() {
	}
	
	public Director(String name, String surname, LocalDate birthDate, String nationality, boolean isProducer,
			Genre favouriteGenre) {
		super(name, surname, birthDate, nationality);
		this.isProducer = isProducer;
		this.favouriteGenre = favouriteGenre;
	}

	public Director(boolean isProducer) {
		this.isProducer = isProducer;
	}
	
	public boolean getIsProducer() {
		return isProducer;
	}

	public void setIsProducer(boolean isProducer) {
		this.isProducer = isProducer;
	}

	public Genre getFavouriteGenre() {
		return favouriteGenre;
	}

	public void setFavouriteGenre(Genre favouriteGenre) {
		this.favouriteGenre = favouriteGenre;
	}
}
