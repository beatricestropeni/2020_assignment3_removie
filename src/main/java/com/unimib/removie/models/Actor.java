package com.unimib.removie.models;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name="Actors")
public class Actor extends Troupe {
	private int height;
	@Column(name = "hair_color")
	private String hairColor;
	@Column(name = "eyes_color")
	private String eyesColor;
	
	public Actor() {
		super();
	}

	public Actor(String name, String surname, LocalDate birthDate, String nationality, int height, String hairColor, String eyesColor) {
		super(name, surname, birthDate, nationality);
		this.height = height;
		this.hairColor = hairColor;
		this.eyesColor = eyesColor;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	public String getEyesColor() {
		return eyesColor;
	}

	public void setEyesColor(String eyesColor) {
		this.eyesColor = eyesColor;
	}
	
}
