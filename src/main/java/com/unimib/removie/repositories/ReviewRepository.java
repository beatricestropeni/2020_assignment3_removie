package com.unimib.removie.repositories;


import com.unimib.removie.models.Review;

public interface ReviewRepository extends Repository<Review, Integer> {
	Iterable<Review> findByUsername(String username);
	Iterable<Review> findByMovie(int movieId);
	Iterable<Review> findByMember(int memberId);
	Iterable<Review> findBySubject(String subject);
	Iterable<Review> findBySubjectPerUser(String subject, String username);

}
