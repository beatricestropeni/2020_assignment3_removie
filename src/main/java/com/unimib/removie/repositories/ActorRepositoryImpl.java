package com.unimib.removie.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.stereotype.Service;
import com.unimib.removie.models.Actor;
import com.unimib.removie.models.Movie;

@Service
public class ActorRepositoryImpl implements ActorRepository {
	private static ActorRepositoryImpl instance;
	private EntityManagerFactory entityManagerFactory;
	
	public ActorRepositoryImpl() {
	    this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.removie");
	}
	
	public static ActorRepositoryImpl getInstance() {
		if(instance == null) 
			instance = new ActorRepositoryImpl();
		return instance;
	}

	@Override
	public Optional<Actor> findById(Integer id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Actor attore = entityManager.find(Actor.class, id);
		entityManager.close();
		return Optional.ofNullable(attore);
	}

	@Override
	public Iterable<Actor> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Actor> attori = entityManager.createQuery("FROM Actor", Actor.class).getResultList();
		entityManager.close();
		return attori;
	}

	@Override
	public void insert(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(actor);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public Iterable<Actor> findByName(String name) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Actor> attori = entityManager.createQuery("FROM Actor WHERE name = :name", Actor.class).setParameter("name", name).getResultList();
		entityManager.close();
		return attori;
	}

	@Override
	public Iterable<Actor> findBySurname(String surname) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Actor> attori = entityManager.createQuery("FROM Actor WHERE UPPER(surname) LIKE UPPER(:surname)", Actor.class).setParameter("surname", surname).getResultList();
		entityManager.close();
		return attori;
	}

	@Override
	public void delete(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(actor) ? actor : entityManager.merge(actor));
		entityManager.getTransaction().commit();
		entityManager.close();
		
	}

	@Override
	public void update(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(actor);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void detach(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.detach(actor);
	}

	//ritorna true se l'attore non è legato a nessuna istanza di film
	@Override
	public boolean isFree(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Actor> attori = entityManager.createQuery("SELECT a FROM Movie m INNER JOIN m.actors a WHERE a.id = :id", Actor.class).setParameter("id", actor.getId()).getResultList();
		entityManager.close();
		return attori.isEmpty();
	}

	//ritorna tutte le istanze di film legate all'attore
	@Override
	public Iterable<Movie> allMovieOfActor(Actor actor) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Movie> film = entityManager.createQuery("SELECT m FROM Movie m INNER JOIN m.actors a WHERE a.id = :id", Movie.class).setParameter("id", actor.getId()).getResultList();
		entityManager.close();
		return film;
	}
	
}
