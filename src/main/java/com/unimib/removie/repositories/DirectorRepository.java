package com.unimib.removie.repositories;

import com.unimib.removie.models.Director;
import com.unimib.removie.models.Movie;

public interface DirectorRepository extends Repository<Director, Integer> {
	public Iterable<Director> findByName(String name);
	public Iterable<Director> findBySurname(String surname);
	public void detach(Director director);
	public boolean isFree(Director director);
	public Iterable<Movie> allMovieOfDirector(Director director);
}