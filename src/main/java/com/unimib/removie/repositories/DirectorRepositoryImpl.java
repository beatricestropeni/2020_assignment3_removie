package com.unimib.removie.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.*;

import org.springframework.stereotype.Service;

import com.unimib.removie.models.Director;
import com.unimib.removie.models.Movie;

@Service
public class DirectorRepositoryImpl implements DirectorRepository {
	private static DirectorRepositoryImpl instance;
	private EntityManagerFactory entityManagerFactory;
	
	public DirectorRepositoryImpl() {
	    this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.removie");
	}
	
	public static DirectorRepositoryImpl getInstance() {
		if(instance == null) 
			instance = new DirectorRepositoryImpl();
		return instance;
	}

	@Override
	public void insert(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(director);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public Optional<Director> findById(Integer id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Director regista = entityManager.find(Director.class, id);
		entityManager.close();
		return Optional.ofNullable(regista);
	}

	@Override
	public Iterable<Director> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Director> registi = entityManager.createQuery("FROM Director", Director.class).getResultList();
		entityManager.close();
		return registi;
	}

	@Override
	public void delete(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(director) ? director : entityManager.merge(director));
		entityManager.getTransaction().commit();
		entityManager.close();
		
	}

	@Override
	public Iterable<Director> findByName(String name) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Director> registi = entityManager.createQuery("FROM Director WHERE name = :name", Director.class).setParameter("name", name).getResultList();
		entityManager.close();
		return registi;
	}

	@Override
	public Iterable<Director> findBySurname(String surname) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Director> registi = entityManager.createQuery("FROM Director WHERE UPPER(surname) LIKE UPPER(:surname)", Director.class).setParameter("surname", surname).getResultList();
		entityManager.close();
		return registi;
	}

	@Override
	public void update(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(director);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void detach(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.detach(director);
		
	}

	//ritorna true se il regista non è legato a nessuna istanza di film
	@Override
	public boolean isFree(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Director> registi = entityManager.createQuery("SELECT d FROM Movie m INNER JOIN m.director d WHERE d.id = :id", Director.class).setParameter("id", director.getId()).getResultList();
		entityManager.close();
		return registi.isEmpty();
	}

	//ritorna tutte le istanze di film legate al regista
	@Override
	public Iterable<Movie> allMovieOfDirector(Director director) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Movie> film = entityManager.createQuery("SELECT m FROM Movie m INNER JOIN m.director d WHERE d.id = :id", Movie.class).setParameter("id", director.getId()).getResultList();
		entityManager.close();
		return film;
	}
}
