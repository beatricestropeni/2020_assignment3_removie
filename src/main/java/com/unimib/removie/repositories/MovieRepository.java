package com.unimib.removie.repositories;

import com.unimib.removie.models.Movie;

public interface MovieRepository extends Repository<Movie, Integer> {

	Iterable<Movie> findByTitleIgnoreCase(String title);

	boolean isRemakeOfOtherFilms(Movie movie);
}
