package com.unimib.removie.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.stereotype.Service;

import com.unimib.removie.models.Review;

@Service
public class ReviewRepositoryImpl implements ReviewRepository {
	private static ReviewRepositoryImpl instance;
	private EntityManagerFactory entityManagerFactory;
	
	public ReviewRepositoryImpl() {
	    this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.removie");
	}
	
	public static ReviewRepositoryImpl getInstance() {
		if(instance == null) 
			instance = new ReviewRepositoryImpl();
		return instance;
	}
	
	@Override
	public void update(Review review) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(review);
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	@Override
	public void insert(Review review) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(review);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public Optional<Review> findById(Integer id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Review review = entityManager.find(Review.class, id);
		entityManager.close();
		return Optional.ofNullable(review);
	}

	@Override
	public Iterable<Review> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("FROM Review", Review.class).getResultList();
		entityManager.close();
		return reviews;
	}

	@Override
	public void delete(Review review) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(review) ? review : entityManager.merge(review));
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	@Override
	public Iterable<Review> findByUsername(String username) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("FROM Review WHERE user.username = :username", Review.class).setParameter("username", username).getResultList();
		entityManager.close();
		return reviews;
	}
	
	@Override
	public Iterable<Review> findByMovie(int movieId) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("FROM Review WHERE movie.id = :id", Review.class).setParameter("id", movieId).getResultList();
		entityManager.close();
		return reviews;
	}
	
	@Override
	public Iterable<Review> findByMember(int memberId) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("SELECT r FROM Review r WHERE r.member.id = :id", Review.class).setParameter("id", memberId).getResultList();
		entityManager.close();
		return reviews;
	}
	
	// Ricerca tutte le recensioni con lo stesso titolo di film o stesso nome o cognome di attore/regista
	@Override
	public Iterable<Review> findBySubject(String subject) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("SELECT r FROM Review r LEFT JOIN r.movie LEFT JOIN r.member WHERE "
				+ "UPPER(r.member.name) LIKE UPPER(:subject) "
				+ "or UPPER(r.member.surname) LIKE UPPER(:subject) "
				+ "or UPPER(r.movie.title) LIKE UPPER(:subject) ", Review.class)
				.setParameter("subject", subject)
				.getResultList();
		entityManager.close();
		return reviews;
	}
	
	// Ricerca tutte le recensioni legate a un utente con lo stesso titolo di film o stesso nome o cognome di attore/regista
	@Override
	public Iterable<Review> findBySubjectPerUser(String subject, String username) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Review> reviews = entityManager.createQuery("SELECT r FROM Review r LEFT JOIN r.movie LEFT JOIN r.member LEFT JOIN r.user WHERE "
				+ "(UPPER(r.member.name) LIKE UPPER(:subject) "
				+ "or UPPER(r.member.surname) LIKE UPPER(:subject) "
				+ "or UPPER(r.movie.title) LIKE UPPER(:subject)) "
				+ "and r.user.username =: username", Review.class)
				.setParameter("subject", subject)
				.setParameter("username", username)
				.getResultList();
		entityManager.close();
		return reviews;
	}
}


