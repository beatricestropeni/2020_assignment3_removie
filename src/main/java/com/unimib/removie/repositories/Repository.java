package com.unimib.removie.repositories;

import java.util.Optional;

public interface Repository<T, I> {
	Optional<T> findById(I id);
	Iterable<T> findAll();
	void insert(T object);
	void delete(T object);
	void update(T object);
}
