package com.unimib.removie.repositories;

import com.unimib.removie.models.User;

public interface UserRepository extends Repository<User, Integer>{
	public User findByUsername(String username);
}
