package com.unimib.removie.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.stereotype.Service;

import com.unimib.removie.models.User;

@Service
public class UserRepositoryImpl implements UserRepository {
	private static UserRepositoryImpl instance;
	private EntityManagerFactory entityManagerFactory;
	
	public UserRepositoryImpl() {
	    this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.removie");
	}

	public static UserRepositoryImpl getInstance() {
		if(instance == null) 
			instance = new UserRepositoryImpl();
		return instance;
	}
	
	@Override
	public void insert(User user) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(user);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public Iterable<User> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<User> utenti = entityManager.createQuery("FROM User", User.class).getResultList();
		entityManager.close();
		return utenti;
	}


	@Override
	public User findByUsername(String username) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<User> utente = entityManager.createQuery("FROM User WHERE username = :username", User.class).setParameter("username", username).getResultList();
		entityManager.close();
		if(utente.isEmpty())
			return null;
		else
			return utente.get(0);
	}


	@Override
	public Optional<User> findById(Integer id) {
		return Optional.empty();
		//non è prevista la ricerca dell'utente tramite id
	}


	@Override
	public void delete(User object) {
		//non è prevista la cancellazione dell'utente
	}


	@Override
	public void update(User object) {	
		//non è prevista la modifica dell'utente
	}
}
