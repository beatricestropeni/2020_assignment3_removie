package com.unimib.removie.repositories;

import com.unimib.removie.models.Actor;
import com.unimib.removie.models.Movie;

public interface ActorRepository extends Repository<Actor, Integer> {
	public Iterable<Actor> findByName(String name);
	public Iterable<Actor> findBySurname(String surname);
	public void detach(Actor actor);
	public boolean isFree(Actor actor);
	public Iterable<Movie> allMovieOfActor(Actor actor);
}
