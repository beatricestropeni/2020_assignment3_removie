package com.unimib.removie.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.stereotype.Service;
import com.unimib.removie.models.Movie;

@Service
public class MovieRepositoryImpl implements MovieRepository {
	private static MovieRepositoryImpl instance;
	private EntityManagerFactory entityManagerFactory;
	
	public MovieRepositoryImpl() {
	    this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.removie");
	}
	
	public static MovieRepositoryImpl getInstance() {
		if(instance == null) 
			instance = new MovieRepositoryImpl();
		return instance;
	}

	@Override
	public void insert(Movie movie) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(movie);
		entityManager.getTransaction().commit();
		entityManager.close();
	}


	@Override
	public void delete(Movie movie) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.contains(movie) ? movie : entityManager.merge(movie));
		entityManager.getTransaction().commit();
		entityManager.close();
	}


	@Override
	public Optional<Movie> findById(Integer id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Movie movie = entityManager.find(Movie.class, id);
		entityManager.close();
		return Optional.ofNullable(movie);
	}

	@Override
	public Iterable<Movie> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Movie> movies = entityManager.createQuery("FROM Movie", Movie.class).getResultList();
		entityManager.close();
		return movies;
	}
	
	@Override
	public Iterable<Movie> findByTitleIgnoreCase(String title) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Movie> movies = entityManager.createQuery("FROM Movie WHERE UPPER(title) LIKE UPPER(:title)", Movie.class).setParameter("title", title).getResultList();
		entityManager.close();
		return movies;
	}

	@Override
	public void update(Movie movie) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(movie);
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	//ritorna true se l'id dell'oggetto movie compare come remake in altri film
	@Override
	public boolean isRemakeOfOtherFilms(Movie movie) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Movie> movies = entityManager.createQuery("SELECT remake FROM Movie m INNER JOIN m.remake remake WHERE remake.id = :id", Movie.class).setParameter("id", movie.getId()).getResultList();
		entityManager.close();
		
		return !movies.isEmpty();
	}
}
