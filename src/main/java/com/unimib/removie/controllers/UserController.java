package com.unimib.removie.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.unimib.removie.models.User;
import com.unimib.removie.repositories.*;

@RestController
public class UserController {
	private static final String ADMIN = "admin";
	private static final String USERNAME = "username";
	private static final String REDIRECT_HOMEPAGE = "redirect:/";
	
	//ritorna il form per la registrazione di un nuovo utente
	@GetMapping(value="/registration" )
	public ModelAndView getRegistrationForm(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) != null || request.getSession().getAttribute(USERNAME) != null) {
			mv.setViewName("access-denied");
		}
		else {
			mv.addObject("user", new User());
			mv.setViewName("registration");
		}
		return mv;
	}
	
	//effettua la registrazione di un nuovo utente, controllando che non esista già quell'username
	@PostMapping(value="/registration")
	public ModelAndView registration(@ModelAttribute User user) {
		ModelAndView mv = new ModelAndView();
		if(UserRepositoryImpl.getInstance().findByUsername(user.getUsername()) != null || user.getUsername().equals(ADMIN)) {
			mv.setViewName("redirect:/registration?error");
		}
		else {
			UserRepositoryImpl.getInstance().insert(user);
			mv.setViewName("redirect:/login");
		}
		return mv;
	}
	
	//ritorna il form per il login dell'utente
	@GetMapping(value="/login")
	public ModelAndView getLoginForm(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(USERNAME) != null || request.getSession().getAttribute(ADMIN) != null) {
			mv.setViewName(REDIRECT_HOMEPAGE);
		}
		else {
			mv.addObject("user", new User());
			mv.setViewName("login");
		}
		return mv;
	}
	
	//effettua il login dell'utente controllando che username e password siano corrette
	@PostMapping(value="/login")
	public ModelAndView login(@ModelAttribute User userForm, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		User userDB = UserRepositoryImpl.getInstance().findByUsername(userForm.getUsername());
		
		
		if(userForm.getUsername().equals(ADMIN) && userForm.getPassword().equals(ADMIN)) {
			request.getSession().setAttribute(ADMIN, ADMIN);
			request.getSession().setAttribute(USERNAME, ADMIN);
			mv.setViewName(REDIRECT_HOMEPAGE);
		}
		else {
			if(userDB != null && userDB.getPassword().equals(userForm.getPassword())) {
				request.getSession().setAttribute(USERNAME, userDB.getUsername());
				mv.setViewName(REDIRECT_HOMEPAGE);
			}
			else {
				mv.setViewName("redirect:/login?error");
			}
		}
		
		return mv;
	}
	
	//effettua il logout dell'utente
	@GetMapping(value="/logout")
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		request.getSession().invalidate();
		mv.setViewName("redirect:/login");
		return mv;
	}
}
