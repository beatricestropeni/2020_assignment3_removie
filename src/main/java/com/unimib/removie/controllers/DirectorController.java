package com.unimib.removie.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.unimib.removie.models.*;
import com.unimib.removie.repositories.*;

@RestController
@RequestMapping("/directors")
@SessionAttributes("review")
public class DirectorController {
	private static final String ADMIN = "admin";
	private static final String ACCESS_DENIED = "access-denied";
	private static final String REDIRECT_DIRECTORS = "redirect:/directors";
	
	@ModelAttribute("allGenres")
	public List<Genre> populateGenres() {
	    return Arrays.asList(Genre.getAllGenre());
	}
	
	//se surname è vuoto torna tutti i registi della tabella, altrimenti cerca per cognome
	@GetMapping(value="")
	public ModelAndView getAllDirector(@RequestParam(name = "surname", defaultValue = "") String surname) {
		ModelAndView mv = new ModelAndView();
		Iterable<Director> directors;
		if(surname.equals(""))
			directors = DirectorRepositoryImpl.getInstance().findAll();
		else
			directors =DirectorRepositoryImpl.getInstance().findBySurname(surname);
		mv.addObject("directors", directors);
		mv.setViewName("directors/directors");
		return mv;
	}
	
	@GetMapping(value="/{id}")
	public ModelAndView getDirectorById(@PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		Optional<Director> d= DirectorRepositoryImpl.getInstance().findById(id);
		Iterable<Review> reviewList = ReviewRepositoryImpl.getInstance().findByMember(id);
		if(d.isPresent()) {
			mv.addObject("director", d.get());
			mv.addObject("reviews", reviewList);
			mv.addObject("movies", DirectorRepositoryImpl.getInstance().allMovieOfDirector(d.get()));
			mv.setViewName("directors/director");
		}
		
		return mv;
	}
	
	@GetMapping(value="/add")
	public ModelAndView getDirectorFormAdd(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			mv.addObject(new Director(true));
			mv.setViewName("directors/addDirector");
		}
		return mv;
	}
	
	@PostMapping(value="/add")
	public ModelAndView addDirector(@ModelAttribute Director director) {
		ModelAndView mv = new ModelAndView();
		DirectorRepositoryImpl.getInstance().insert(director);
		mv.setViewName(REDIRECT_DIRECTORS);
		return mv;
	}
	
	@GetMapping(value="/{id}/edit")
	public ModelAndView getDirectorFormEdit(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			Optional<Director> director= DirectorRepositoryImpl.getInstance().findById(id);
			if(director.isPresent()) {
				DirectorRepositoryImpl.getInstance().detach(director.get());
				mv.addObject(director.get());
				mv.setViewName("directors/editDirector");
			}
		}
		return mv;
	}
	
	@PostMapping(value="/{id}/edit")
	public ModelAndView editDirector(@ModelAttribute("director") Director director, @PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		director.setId(id);
		DirectorRepositoryImpl.getInstance().update(director);
		mv.setViewName(REDIRECT_DIRECTORS);
		return mv;
	}
	
	//prima di eliminare il regista controllo che non esistano film che ha diretto, altrimenti elimino lui e le sue recensioni
	@GetMapping(value="/{id}/delete")
	public ModelAndView deleteDirector(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			Optional<Director> director=DirectorRepositoryImpl.getInstance().findById(id);
			if(director.isPresent()) {
				if(DirectorRepositoryImpl.getInstance().isFree(director.get())) {
					this.deleteReviews(director.get());
					DirectorRepositoryImpl.getInstance().delete(director.get());
					mv.setViewName(REDIRECT_DIRECTORS);
				}
				else
					mv.setViewName(REDIRECT_DIRECTORS+"?error=true");
			}
		}
		return mv;
	}
	
	private void deleteReviews(Director director) {
		Iterable<Review> reviewList;
		reviewList = ReviewRepositoryImpl.getInstance().findByMember(director.getId());
		if(reviewList != null) {
			Iterator<Review> itr=reviewList.iterator();
			while(itr.hasNext())
				ReviewRepositoryImpl.getInstance().delete(itr.next());
		}
	}
	
	// Ritorna il form per l'aggiunta di una recensione al regista con id = id (solo se utente)
	@GetMapping(value="/{id}/review/add")
	public ModelAndView getFormDirectorReview(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
			if (request.getSession().getAttribute("username") != null && request.getSession().getAttribute(ADMIN) == null) {
				Optional<Director> director = DirectorRepositoryImpl.getInstance().findById(id);
				User user = UserRepositoryImpl.getInstance().findByUsername((String) request.getSession().getAttribute("username"));
				if(director.isPresent()) {
				mv.addObject("review", new Review(director.get(), null, null, 3, user));
				mv.setViewName("directors/add-director-review");
				}
			}
			else {
				mv.setViewName(ACCESS_DENIED);
				}
		
		return mv;
	}
	
	// Aggiunge una recensione al regista con id = id
	@PostMapping(value="/{id}/review/add")
	public ModelAndView addDirectorReview(@PathVariable int id, @ModelAttribute("review") Review review, HttpServletRequest request,
			SessionStatus status) {
		ModelAndView mv = new ModelAndView();
		ReviewRepositoryImpl.getInstance().insert(review);
		status.setComplete();
		mv.setViewName(REDIRECT_DIRECTORS+"/{id}");
		return mv;
	}

	// Limita i campi di una recensione inseribili dall'utente al rating e al testo
	@InitBinder("review")
	public void initBinder(WebDataBinder binder) {
	    binder.setAllowedFields("rating","description");
	}
}
