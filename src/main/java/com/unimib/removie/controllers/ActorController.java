package com.unimib.removie.controllers;

import java.util.Iterator;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.unimib.removie.models.Actor;
import com.unimib.removie.models.Review;
import com.unimib.removie.models.User;
import com.unimib.removie.repositories.ActorRepositoryImpl;
import com.unimib.removie.repositories.ReviewRepositoryImpl;
import com.unimib.removie.repositories.UserRepositoryImpl;

@RestController
@RequestMapping("/actors")
@SessionAttributes("review")
public class ActorController {
	private static final String ADMIN = "admin";
	private static final String ACCESS_DENIED = "access-denied";
	private static final String REDIRECT_ACTORS = "redirect:/actors";
	
	//se surname è vuoto torna tutti gli attori della tabella, altrimenti cerca per cognome
	@GetMapping(value="")
	public ModelAndView getAllActor(@RequestParam(name = "surname", defaultValue = "") String surname) {
		ModelAndView mv = new ModelAndView();
		Iterable<Actor> actors;
		if(surname.equals(""))
			actors = ActorRepositoryImpl.getInstance().findAll();
		else
			actors = ActorRepositoryImpl.getInstance().findBySurname(surname);
		mv.addObject("actors", actors);
		mv.setViewName("actors/actors");
		return mv;
	}
	
	@GetMapping(value="/{id}")
	public ModelAndView getActorById(@PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		Optional<Actor> a = ActorRepositoryImpl.getInstance().findById(id);
		Iterable<Review> reviewList = ReviewRepositoryImpl.getInstance().findByMember(id);
		if(a.isPresent()){
			mv.addObject("actor", a.get());
			mv.addObject("reviews", reviewList);
			mv.addObject("movies", ActorRepositoryImpl.getInstance().allMovieOfActor(a.get()));
			mv.setViewName("actors/actor");
		}
		
		return mv;
	}
	
	@GetMapping(value="/add")
	public ModelAndView getActorFormAdd(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			mv.addObject(new Actor());
			mv.setViewName("actors/addActor");
		}
		return mv;
	}
	
	@PostMapping(value="/add")
	public ModelAndView addActor(@ModelAttribute Actor actor) {
		ModelAndView mv = new ModelAndView();
		ActorRepositoryImpl.getInstance().insert(actor);
		mv.setViewName(REDIRECT_ACTORS);
		return mv;
	}
	
	@GetMapping(value="/{id}/edit")
	public ModelAndView getActorFormEdit(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			
			Optional<Actor> actor= ActorRepositoryImpl.getInstance().findById(id);
			if(actor.isPresent()){
				ActorRepositoryImpl.getInstance().detach(actor.get());
				mv.addObject(actor.get());
				mv.setViewName("actors/editActor");
				
			}
		}
		return mv;
	}
	
	@PostMapping(value="/{id}/edit")
	public ModelAndView editActor(@ModelAttribute("actor") Actor actor, @PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		actor.setId(id);
		ActorRepositoryImpl.getInstance().update(actor);
		mv.setViewName(REDIRECT_ACTORS);
		return mv;
	}
	
	// Ritorna il form per l'aggiunta di una recensione all'attore con id = id (solo se utente)
	@GetMapping(value="/{id}/review/add")
	public ModelAndView getFormActorReview(@PathVariable int id, HttpServletRequest request,
			SessionStatus status) {
		ModelAndView mv = new ModelAndView();
			if (request.getSession().getAttribute("username") != null && request.getSession().getAttribute(ADMIN) == null) {
				Optional<Actor> actor = ActorRepositoryImpl.getInstance().findById(id);
				User user = UserRepositoryImpl.getInstance().findByUsername((String) request.getSession().getAttribute("username"));
				if (actor.isPresent()) {
					mv.addObject("review", new Review(actor.get(), null, null, 3, user));
					mv.setViewName("actors/add-actor-review");
				}
			}
			else {
				mv.setViewName(ACCESS_DENIED);
				}
		
		return mv;
	}
	
	// Aggiunge una recensione all'attore con id = id
	@PostMapping(value="/{id}/review/add")
	public ModelAndView addActorReview(@PathVariable int id, @ModelAttribute("review") Review review, HttpServletRequest request,
		SessionStatus status) {
		ModelAndView mv = new ModelAndView();	
		ReviewRepositoryImpl.getInstance().insert(review);
		status.setComplete();
		mv.setViewName(REDIRECT_ACTORS+"/{id}");
		return mv;
	}
	
	//prima di eliminare l'attore controllo che non esistano film in cui ha recitato, altrimenti elimino lui e le sue recensioni
	@GetMapping(value="/{id}/delete")
	public ModelAndView deleteActor(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			Optional<Actor> actor=ActorRepositoryImpl.getInstance().findById(id);
			if(actor.isPresent()){
				if(ActorRepositoryImpl.getInstance().isFree(actor.get())) {
					this.deleteReviews(actor.get());
					ActorRepositoryImpl.getInstance().delete(actor.get());
					mv.setViewName(REDIRECT_ACTORS);
				}
				else {
					mv.setViewName(REDIRECT_ACTORS+"?error=true");
				}
			}
		}
		return mv;
	}
	
	private void deleteReviews(Actor actor) {
		Iterable<Review> reviewList;
		reviewList = ReviewRepositoryImpl.getInstance().findByMember(actor.getId());
		if(reviewList != null) {
			Iterator<Review> itr=reviewList.iterator();
			while(itr.hasNext())
				ReviewRepositoryImpl.getInstance().delete(itr.next());
		}
	}
	
	// Limita i campi di una recensione inseribili dall'utente al rating e al testo 
	@InitBinder("review")
	public void initBinder(WebDataBinder binder) {
	    binder.setAllowedFields("rating","description");
	}
}
