package com.unimib.removie.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomepageController {
	
	@GetMapping(value="")
	public ModelAndView getHomepage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("homepage");
		return mv;
	}
	
}
