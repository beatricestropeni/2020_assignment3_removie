package com.unimib.removie.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.unimib.removie.models.Review;
import com.unimib.removie.repositories.ReviewRepositoryImpl;


@RestController
@RequestMapping("/profile/reviews")
@SessionAttributes("review")
public class ProfileController {
	private static final String USERNAME = "username";
	private static final String ADMIN = "admin";
	private static final String ACCESS_DENIED = "access-denied";
	private static final String REDIRECT_PROFILE_REVIEWS = "redirect:/profile/reviews";	
	
	// Ritorna tutte le review. Lista filtrabile per (nome o cognome o titolo) = "subject".
	@GetMapping(value="")
	public ModelAndView getAllUserReviews(@RequestParam(name = "subject", defaultValue = "") String subject, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		// Admin: ritorna tutte le review di tutti gli utenti
		if(request.getSession().getAttribute(ADMIN) != null)
		{
			Iterable<Review> reviewList;
			if(subject.equals(""))
				reviewList = ReviewRepositoryImpl.getInstance().findAll();
			else
				reviewList = ReviewRepositoryImpl.getInstance().findBySubject(subject);
 			
 			mv.addObject("reviewList", reviewList);
 			mv.setViewName("profile/profile-reviews");
		}
		// User: ritorna solo le review dell'utente
		else if(request.getSession().getAttribute(USERNAME) != null) {
			String username = (String) request.getSession().getAttribute(USERNAME);
			Iterable<Review> reviewList;
			if(subject.equals(""))
				reviewList = ReviewRepositoryImpl.getInstance().findByUsername(username);
			else
				reviewList = ReviewRepositoryImpl.getInstance().findBySubjectPerUser(subject, username);
 		
			mv.addObject("reviewList", reviewList);
			mv.setViewName("profile/profile-reviews");
		}

		else
		{
			mv.setViewName(ACCESS_DENIED);
		}
		return mv;	
	}
	
	
	// Cancella una recensione
	@GetMapping(value="/{id}/delete")
	public ModelAndView deleteReview(@PathVariable Integer id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		Optional<Review> review = ReviewRepositoryImpl.getInstance().findById(id);
		
		if (review.isPresent()) {
			// Admin: cancella qualsiasi recensione
			if (request.getSession().getAttribute(ADMIN) != null) {
				ReviewRepositoryImpl.getInstance().delete(review.get());
				mv.setViewName(REDIRECT_PROFILE_REVIEWS);
				}
			// Utente: cancella solo le proprie recensioni
			else if (request.getSession().getAttribute(USERNAME) != null) {
				if (request.getSession().getAttribute(USERNAME) == review.get().getUser().getUsername()) {
					ReviewRepositoryImpl.getInstance().delete(review.get());
					mv.setViewName(REDIRECT_PROFILE_REVIEWS);
				}
				else {
					mv.setViewName(REDIRECT_PROFILE_REVIEWS+"?error");
				}
			}
		}
		
		return mv;
	}
	
	// Ritorna un form per la modifica della propria recensione (solo se utente)
	@GetMapping(value = "/{id}/edit")
	public ModelAndView getReviewFormEdit(@PathVariable Integer id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		// Utente
		if(request.getSession().getAttribute(USERNAME) != null && request.getSession().getAttribute(ADMIN) == null) {
			Optional<Review> review = ReviewRepositoryImpl.getInstance().findById(id);
			// Utente non proprietario
			if(review.isPresent()) {
				if (request.getSession().getAttribute(USERNAME) != review.get().getUser().getUsername())
					mv.setViewName(ACCESS_DENIED);
				
				else {
					mv.addObject("review", review.get());
					mv.setViewName("profile/edit-review");
				}
			}
		}
		else
			mv.setViewName(ACCESS_DENIED);
		
		return mv;
	}
	
	// Modifica una recensione
	@PostMapping(value = "/{id}/edit")
	public ModelAndView editReview(@PathVariable Integer id, @ModelAttribute("review") Review review, SessionStatus status) {
		ModelAndView mv = new ModelAndView();
		review.setId(id);
		ReviewRepositoryImpl.getInstance().update(review);
		status.setComplete();
		mv.setViewName(REDIRECT_PROFILE_REVIEWS);
		return mv;
	}
	
	// Limita i campi di una recensione modificabili dall'utente al rating e al testo 
	@InitBinder("review")
	public void initBinder(WebDataBinder binder) {
	    binder.setAllowedFields("rating","description");
	}
}
