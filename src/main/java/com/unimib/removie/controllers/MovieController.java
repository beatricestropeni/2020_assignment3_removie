package com.unimib.removie.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.unimib.removie.models.Actor;
import com.unimib.removie.models.Director;
import com.unimib.removie.models.Genre;
import com.unimib.removie.models.Movie;
import com.unimib.removie.models.Review;
import com.unimib.removie.models.User;
import com.unimib.removie.repositories.*;

@RestController
@RequestMapping("/movies")
@SessionAttributes("review")
public class MovieController {
	private static final String ADMIN = "admin";
	private static final String ACCESS_DENIED = "access-denied";
	private static final String REDIRECT_MOVIES = "redirect:/movies";
	
	@ModelAttribute("allGenres")
	public List<Genre> populateGenres() {
	    return Arrays.asList(Genre.getAllGenre());
	}
	
	@ModelAttribute("allDirectors") 
	public Iterable<Director> populateDirectors() {
		return DirectorRepositoryImpl.getInstance().findAll();
	}
	
	@ModelAttribute("allMovies")
	public Iterable<Movie> populateMovies() {
		return MovieRepositoryImpl.getInstance().findAll();
	}
	
	@ModelAttribute("allActors")
	public Iterable<Actor> populateActors() {
		return ActorRepositoryImpl.getInstance().findAll();
	}
	
	//ritorna tutti i film, o quelli con titolo = title (ignoreCase)
	@GetMapping(value="")
	public ModelAndView getMovies(@RequestParam(name = "title", defaultValue = "") String title) {
		ModelAndView mv = new ModelAndView();
		Iterable<Movie> movies;
		
		if(title.equals("")) {
			movies = MovieRepositoryImpl.getInstance().findAll();
		}
		else {
			movies = MovieRepositoryImpl.getInstance().findByTitleIgnoreCase(title);
		}
		
		mv.addObject("movies", movies);
		mv.setViewName("movies/movies");
		return mv;
	}
	
	//ritorna il film con id = id
	@GetMapping(value="/{id}")
	public ModelAndView getMovieById(@PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		Optional<Movie> movie = MovieRepositoryImpl.getInstance().findById(id);
		Iterable<Review> reviewList = ReviewRepositoryImpl.getInstance().findByMovie(id);
		if(movie.isPresent()) {
			mv.addObject(movie.get());
			mv.addObject("reviews", reviewList);
			mv.setViewName("movies/movie");
		}
		
		return mv;
	}
	
	//ritorna il form per l'aggiunta di un nuovo film
	@GetMapping(value="/add")
	public ModelAndView getAddMovieForm(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		
		if(request.getSession().getAttribute(ADMIN) == null) {
			mv.setViewName(ACCESS_DENIED);
		}
		else {
			mv.addObject(new Movie());
			mv.setViewName("movies/add-movie");
		}
		return mv;
	}
	
	//effettua l'inserimento del film, dopo aver fatto il parsato il json in un oggetto Movie
	@PostMapping(value="/add")
	public void addMovie(@RequestBody String json) {		
		MovieRepositoryImpl.getInstance().insert(parseJsonIntoAMovie(json));
	}
	
	//ritorna il form per la modifica di un film
	@GetMapping(value="/{id}/edit")
	public ModelAndView getEditMovieForm(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		
		Optional<Movie> movie = MovieRepositoryImpl.getInstance().findById(id);
		if(movie.isPresent()) {
			
			if(request.getSession().getAttribute(ADMIN) == null) {
				mv.setViewName(ACCESS_DENIED);
			}
			
			mv.addObject(movie.get());
			mv.setViewName("movies/edit-movie");
		}
		return mv;
	}
	
	//effettua la modifica del film, dopo aver fatto il parsato il json in un oggetto Movie
	@PostMapping(value="/{id}/edit")
	public void editMovie(@RequestBody String json, @PathVariable int id) {
		Movie parsedMovie = parseJsonIntoAMovie(json);
		parsedMovie.setId(id);
		MovieRepositoryImpl.getInstance().update(parsedMovie);
		
	}
	
	//converte un json (che contiene i dati del film) in un oggetto Movie
	public Movie parseJsonIntoAMovie(String json) {
		JSONObject jsonObject = new JSONObject(json);  
		JSONArray actorsId = jsonObject.getJSONArray("actors");
		List<Actor> actors = new ArrayList<>();
		Optional<Movie> remakeOptional;
		Movie remake = null;
		Optional<Actor> actor;
		Director director = null;
		int actorId = 0;
		
		//ricerca attori a partire dall'id
		for (int i = 0; i < actorsId.length(); i++) {
			actorId = Integer.parseInt((String)actorsId.get(i));
			actor = ActorRepositoryImpl.getInstance().findById(actorId);
			if(actor.isPresent())
				actors.add(actor.get());
		}
		
		//ricerca regista a partire dall'id
		int directorId = Integer.parseInt(jsonObject.getString("directorId"));
		Optional<Director> directorOptional = DirectorRepositoryImpl.getInstance().findById(directorId);
		if(directorOptional.isPresent())
			director = directorOptional.get();
			
		//ricerca remake, se è stato inserito, a partire dall'id
		if(!jsonObject.isNull("remakeId")) {
			remakeOptional = MovieRepositoryImpl.getInstance().findById(Integer.parseInt((String)jsonObject.get("remakeId")));
			if(remakeOptional.isPresent()) {
				remake = remakeOptional.get();
			}
		}
		
		//estrarre altri dati del film
		String title = jsonObject.getString("title");
		int runningTime = Integer.parseInt(jsonObject.getString("runningTime"));
		Genre genre = Genre.getGenreByName(jsonObject.getString("genre"));
		String productionCompany = jsonObject.getString("productionCompany");
		int releaseDate = Integer.parseInt(jsonObject.getString("releaseDate"));
		String plot = jsonObject.getString("plot");
		
		return new Movie(title, runningTime, genre, productionCompany, releaseDate, plot, remake, director, actors);
		
	}
	
	//cancellazione di un film (solo se non è remake di altri film), con le relative recensione associate
	@GetMapping(value="/{id}/delete")
	public ModelAndView deleteMovie(@PathVariable int id) {
		ModelAndView mv = new ModelAndView();
		Optional<Movie> movieOptional = MovieRepositoryImpl.getInstance().findById(id);
		List<Review> reviewList;
		
		
		if(movieOptional.isPresent()){
			Movie movie = movieOptional.get();
			if(MovieRepositoryImpl.getInstance().isRemakeOfOtherFilms(movie)) {
				mv.setViewName(REDIRECT_MOVIES+"?error=true");
			}
			else {
				reviewList = StreamSupport.stream(ReviewRepositoryImpl.getInstance().findByMovie(movie.getId()).spliterator(), false).collect(Collectors.toList());
				
				if(reviewList != null) {
					for(int i = 0; i < reviewList.size(); i++) {
						ReviewRepositoryImpl.getInstance().delete(reviewList.get(i));
					}
				}
				
				MovieRepositoryImpl.getInstance().delete(movie);
				mv.setViewName(REDIRECT_MOVIES);
			}
		}
		
		return mv;
	}
	
	// Ritorna il form per l'aggiunta di una recensione al film con id = id (solo se utente)
	@GetMapping(value="/{id}/review/add")
	public ModelAndView getFormMovieReview(@PathVariable int id, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		if (request.getSession().getAttribute("username") != null && request.getSession().getAttribute(ADMIN) == null) {
			User user = UserRepositoryImpl.getInstance().findByUsername((String) request.getSession().getAttribute("username"));
			Optional<Movie> movie = MovieRepositoryImpl.getInstance().findById(id);			
			if(movie.isPresent()) {
				mv.addObject("review", new Review(null, movie.get(), null, 3, user));
				mv.addObject("movie", movie.get());
				mv.setViewName("movies/add-movie-review");
				}
		}
		else {
			mv.setViewName(ACCESS_DENIED);
			}
		return mv;
	}
	
	// Aggiunge una recensione al film con id = id
	@PostMapping(value="/{id}/review/add")
	public ModelAndView addMovieReview(@PathVariable int id, @ModelAttribute("review") Review review, HttpServletRequest request,
			SessionStatus status) {
		ModelAndView mv = new ModelAndView();
		ReviewRepositoryImpl.getInstance().insert(review);
		status.setComplete();
		mv.setViewName(REDIRECT_MOVIES+"/{id}");
		return mv;
	}
	
	// Limita i campi di una recensione inseribili dall'utente al rating e al testo
	@InitBinder("review")
	public void initBinder(WebDataBinder binder) {
	    binder.setAllowedFields("rating","description");
	}
}
