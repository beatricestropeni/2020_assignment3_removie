# Assignment 3 - Corso di Processo e Sviluppo del Software - reMovie

## Membri

- Beatrice Stropeni 830017
- Alessandra Rota 829775
- Sofia Zonari 829741

## Come installare

- Andare in **Project Overview > Releases**, selezionare l'ultima versione e scaricare il file "source code" (potete scegliere l'estensione che preferite). 
- Successivamente estrarre i file dalla cartella compressa e eseguire il file jar da riga di comando. 

N.B. per poter visualizzare il database già popolato NON spostare il file .jar in un altra cartella, altrimenti il programma creerà un nuovo database.

```
java -jar removie-0.0.1-20210109.101001-1.jar
```

- Aprire poi il browser ed andare all'indirizzo "http://localhost:8080/" per poter visualizzare l'applicazione.
